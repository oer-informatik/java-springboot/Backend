## Das SpringBoot-Framework installieren und die IDE konfigurieren

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/sbb00_springboot-installieren</span>

> **tl/dr;** _(ca. 4 min Lesezeit): Vorbereiten der Entwicklungsumgebung, Plugins und ein Build-Tool (Maven/Gradle) für [SpringBoot-Projekt](https://oer-informatik.de/springboot-projekt): Installation einer aktuellen Java LTS JDK, VSCode als IDE, die für Java/SpringBoot nötigen Plugins, überprüfen der Gradle/Maven Version. Dieser Artikel ist ein Teil der Artikelserie zu einem [SpringBoot-Projekt](https://oer-informatik.de/springboot-projekt). Weiter geht es dann mit dem [Initialisieren des Projekts](https://oer-informatik.de/sbb01_initialisieren-des-projekts)._

### Installationen und Voraussetzungen

Wir benötigen das _Java Development Kit_, ein Buildtool (_Gradle_ oder _Maven_), eine Entwicklungsumgebung und optional die Versionsverwaltung _git_. Konkret werden folgende Versionen vorgeschlagen:

#### Eine aktuelle Java JDK

Java Entwicklungsumgebung installieren https://www.java.com/de/download/help/download_options_de.html

Oracle veröffentlicht [alle halbe Jahre eine neue Version](https://www.oracle.com/java/technologies/java-se-support-roadmap.html) des _Java Development Kit_ (JDK), für die es jeweils ein halbes Jahr Updates gibt. Um nicht in dieser schnellen Taktung aktualisieren zu müssen, empfehle ich, nicht die aktuellste JDK-Version zu verwenden, sondern die jeweils jüngste Version mit _Long Term Support_. Derzeit ist dies die Version 21 (mit Updates bis 2028), im September 2025 erscheint die nächste LTS Version. 

-  Oracle _Java JDK LTS_ (es wird die aktuelle _long term support_ Version empfohlen, zum Zeitpunkt der Tutorialentstehung ist das Java 21 Ende September 2025 folgt Java25 LTS): https://www.oracle.com/java/technologies/downloads/#java21

   Hilfe für den [Installationsprozess gibt es hier](https://www.java.com/de/download/help/download_options_de.html)

- OpenJDK LTS von anderen Herstellern: Da Oracle für die OpenSource-(GPLv2)-Variante der OpenJDK keinen Langzeitsupport anbieten, haben [eine Reihe anderer Herstellern](https://de.wikipedia.org/wiki/OpenJDK#OpenJDK-Varianten) das übernommen. Es gibt beispielsweise OpenJDKs mit LTS von [Adoptikum](https://adoptium.net/de/) oder [RedHat](https://docs.redhat.com/en/documentation/red_hat_build_of_openjdk/21)

- [http://jdk.java.net/](OpenJDK von Oracle): Für die neuste Java-Variante Oracle unter GPL gibt es nur ein halbes Jahr Support und es existiert keine LTS-Version. Wer aktuelle Features braucht und kein Problem hat, im Halbjahrestakt zu aktualisieren kann diese Option wählen.

Nach der Installation müssen die Umgebungsvariablen unbedingt gesetzt werden für `JAVA\_HOME` und `PATH`. Für Windows: Einstellungen / Systemumgebungsvariablen bearbeiten / Umgebungsvariablen / Eintragen des Pfads in `PATH`. 

![Zuerst muss die Systemvariable "JAVA_HOME" erstellt werden, dann bei PATH als %JAVA_HOME%\bin\ eingetragen werden.](images/00-umgebungsvariablen.png)
Details und Infos für andere OS finden sich in der Anleitung unter: [https://www.java.com/de/download/help/path.xml]().

Hat die Installation geklappt und ist die Java JDK im `PATH`? Überprüfen wir es in der Konsole:

<span class="tabrow" >
   <button class="tablink" data-tabgroup="shell" data-tabid="bash" onclick="openTabsByDataAttr('bash', 'shell')">Bash/\*nix</button>
   <button class="tablink tabselected" data-tabgroup="shell" data-tabid="powershell" onclick="openTabsByDataAttr('powershell', 'shell')">PowerShell/Windows</button>
   <button class="tablink" data-tabgroup="shell" data-tabid="all" onclick="openTabsByDataAttr('all', 'shell')">_all_</button>
    <button class="tablink" data-tabgroup="shell" data-tabid="none" onclick="openTabsByDataAttr('none', 'shell')">_none_</button>
</span>

<span class="tabs" data-tabgroup="shell" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="shell" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="shell" data-tabid="bash" style="display:none">

Unter Debian/Linux:


Welche Version wurde installiert und hat die Installation geklappt (im Terminal bzw. der PowerShell)?

```bash
$ java --version
```

```
java 21.0.1 2023-10-17 LTS
Java(TM) SE Runtime Environment (build 21.0.1+12-LTS-29)
Java HotSpot(TM) 64-Bit Server VM (build 21.0.1+12-LTS-29, mixed mode, sharing)
```

Einträge in den Umgebungsvariablen überprüfen:

```bash
$ echo $PATH
```

(Es wird eine lange Liste ausgegeben, in der irgendwo der Pfad zur aktuellen JDK stehen sollte.)

```bash
$ echo $JAVA_HOME
```

(Es sollte der Pfad zur aktuellen JDK ausgegeben werden.)

</span>

<span class="tabs" data-tabgroup="shell" data-tabid="powershell">

```powershell
PS C:\> java -version
```

Die Ausgabe sollte etwa sein:

```
java version "21.0.1" 2023-10-17 LTS
Java(TM) SE Runtime Environment (build 21.0.1+12-LTS-29)
Java HotSpot(TM) 64-Bit Server VM (build 21.0.1+12-LTS-29, mixed mode, sharing)
```

Manche Programme greifen über die Umgebungsvariable `JAVA_HOME` auf Java zu. Wie ist diese gesetzt?

```powershell
PS C:\> Write-Output $Env:JAVA_HOME 
```

Wird eine ältere Version angezeigt, obwohl bereits eine aktuellere installiert ist und auch bei `java -v` korrekt ausgegeben wird? Dann muss die Einstellung von `JAVA_HOME` oder `PATH` überprüft werden (ggf. auch die PowerShell nach Änderungen nochmal schließen).

Welche Java-Versionen stehen im `PATH`? Die PowerShell findet so alle Pfade, die Java enthalten:

```powershell
Write-Output $Env:Path | Out-String -Stream | ForEach-Object { $_.Split(";") } | Select-String -Pattern "java" -SimpleMatch
```

Das Ergebnis kann auch eine längere Liste sein:

```
C:\Program Files\Java\jdk-21\bin\
C:\Program Files (x86)\Common Files\Oracle\Java\javapath
C:\Program Files (x86)\Common Files\Oracle\Java\java8path
C:\Program Files\Java\jdk-11.0.4\bin
C:\Program Files\NetBeans-11.1\netbeans\java\maven\bin
C:\Program Files\Java\jdk-21
```

</span>

(Sofern hier noch `java version "1.8.0_261"` oder eine Version \<11.0 gemeldet werden sollte, muss auf jeden Fall überprüft werden, ob ein aktuelles Java installiert ist und dies ggf. nachgeholt werden! Diese Version ist definitiv zu alt!)

#### Eine SpringBoot-fähige IDE, z.B. VS Codium

Es gibt mehrere Entwicklungsumgebungen (IDE), die eine gute SpringBoot-Unterstützung umsetzen:

- Visual Studio Code (bzw. besser: die Opensource- und telemetriefreie Variante VS Codium) mit dem _Java Extension Pack_ und dem _SpringBoot Extension Pack_  - Download z.B. hier: [VSCodium - für Win z.B. VSCodium-win32-x64-1.81.1.23222.zip ("Show all 116 assets" aufklappen!)](https://github.com/VSCodium/vscodium/releases)

- [Spring Tool Suite (STS), basierend auf der IDE Eclipse](https://spring.io/tools)

- IntelliJ IDEA Ultimate (kostenpflichtig, mit education-account aber kostenfrei nutzbar)

- [Theia IDE](https://theia-ide.org/)

- [Netbeans](https://netbeans.apache.org/download/), jedoch in der aktuellen Version ist das komfortable Plugin "NB Springboot" nicht vorhanden

In den folgenden Beispielen nutze ich VS Codium als IDE, da es kostenlos verfügbar und telemetriefrei ist. Wer über eine IntelliJ-Lizenz verfügt kann von der größeren SpringBoot-Integration dort profitieren.


#### Die nötigen Extensions installieren

Das ["Extension Pack for Java"](https://open-vsx.org/extension/vscjava/vscode-java-pack):

![Unter "Extensions" "Extension Pack for Java" wählen](images/VSCode/01_InstallJavaExtensionPack.png)

Das ["SpringBoot Extension Pack"](https://open-vsx.org/extension/VMware/vscode-boot-dev-pack):

![Unter "Extensions" SpringBoot Extension Pack wählen](images/VSCode/00_InstallSpringBootExtensionPack.png)

#### Build-Tool installieren (Maven / Gradle)

<span class="tabrow" >
   <button class="tablink" data-tabgroup="buildTool" data-tabid="maven" onclick="openTabsByDataAttr('maven', 'buildTool')">Maven</button>
   <button class="tablink tabselected" data-tabgroup="buildTool" data-tabid="gradle" onclick="openTabsByDataAttr('gradle', 'buildTool')">Gradle</button>
   <button class="tablink" data-tabgroup="buildTool" data-tabid="all" onclick="openTabsByDataAttr('all', 'buildTool')">_all_</button>
    <button class="tablink" data-tabgroup="buildTool" data-tabid="none" onclick="openTabsByDataAttr('none', 'buildTool')">_none_</button>
</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="maven" style="display:none">

Das Buildtool Maven war lange Zeit der Defaultweg für SpringBoot. In der Regel ist es in den modernen IDEs direkt enthalten - es kommt aber vor, dass Projekte direkt über die Konsole ausgeführt werden sollen. Dazu ist es erforderlich, dass eine Maven Version direkt im `PATH` gefunden wird (das kann im Terminal/PowerShell mit `mvn -v` geprüft werden).

Die Maven-Binaries lass sich auf [dieser Seite](https://maven.apache.org/download.cgi) laden, im passenden Pfad für Programme entpacken und entsprechend im `PATH` eingetragen. (Windows Einstellungen Systemumgebungsvariablen bearbeiten / Umgebungsvariablen / Eintragen des Pfads in PATH, Details [siehe z.B. hier](https://maven.apache.org/install.html)).

Nach einem Neustart des Terminals bzw. der IDE sollte Maven gefunden werden:

```bash
mvn -v
```

Sollte etwa folgendes zurückgeben:

```
Apache Maven 3.8.6 (84538c9988a25aec085021c365c560670ad80f63)
Maven home: C:\Program Files\apache-maven-bin\apache-maven-3.8.6
Java version: 11.0.4, vendor: Oracle Corporation, runtime: C:\Program Files\Java\jdk-11.0.4
Default locale: de_DE, platform encoding: Cp1252
OS name: "windows 10", version: "10.0", arch: "amd64", family: "windows"
```

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="gradle" >

Seit der letzten Version nutzt der SpringInitializr per Default das Buildtool Gradle. In der Regel ist es in den modernen IDEs direkt enthalten - es kommt aber vor, dass Projekte direkt über die Konsole ausgeführt werden sollen. Dazu ist es erforderlich, dass eine Gradle Version direkt im `PATH` gefunden wird (das kann im Terminal/PowerShell mit `gradle -v` geprüft werden).

Die Gradle-Binaries lass sich auf [dieser Seite](https://docs.gradle.org/current/userguide/installation.html) laden, im passenden Pfad für Programme entpacken (Win: `C:/Gradle`) und entsprechend im `PATH` eingetragen. (Windows Einstellungen Systemumgebungsvariablen bearbeiten / Umgebungsvariablen / Eintragen des Pfads in PATH, Details [siehe z.B. hier (nach "PATH" suchen und eigenes Betriebssystem wählen)](https://docs.gradle.org/current/userguide/installation.html)).

Nach einem Neustart des Terminals bzw. der IDE sollte Gradle gefunden werden:

```bash
gradle -v
```

Sollte etwa folgendes zurückgeben:

```
Welcome to Gradle 8.3!

Here are the highlights of this release:
 - Faster Java compilation
 - Reduced memory usage
 - Support for running on Java 20

For more details see https://docs.gradle.org/8.3/release-notes.html


------------------------------------------------------------
Gradle 8.3
------------------------------------------------------------

Build time:   2023-08-17 07:06:47 UTC
Revision:     8afbf24b469158b714b36e84c6f4d4976c86fcd5

Kotlin:       1.9.0
Groovy:       3.0.17
Ant:          Apache Ant(TM) version 1.10.13 compiled on January 4 2023
JVM:          21.0.1 (Oracle Corporation 21.0.1+12-LTS-29)
OS:           Windows 10 10.0 amd64
```

Damit sich innerhalb der IDE Gradle komfortabel bedienen lässt, sollte das Plugin "Gradle for Java" installiert werden:

![Das Plugin heißt "Gradle for Java" von vscjava](images/VSCode/01_InstallGradleForJava.png)

</span>



### Nächster Schritt

Dieser Artikel ist ein Teil der Artikelserie zu einem [SpringBoot-Projekt](https://oer-informatik.de/springboot-projekt).

Weiter geht es dann mit dem [Initialisieren des Projekts](https://oer-informatik.de/sbb01_initialisieren-des-projekts).

### Links und weitere Informationen

- OpenJDK: http://jdk.java.net/

- VCCodium: https://github.com/VSCodium/vscodium/releases

- SpringBoot: https://spring.io/projects/spring-boot/
