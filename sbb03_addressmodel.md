## Persistenzschicht und Modell

<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/sbb03_addressmodel</span>

> **tl/dr;** _(ca. 6 min Lesezeit): Es werden die Abhängigkeiten für ein Model angefügt, eine Modelklasse erstellt und annotiert. Die OOP-Hintergründe für Repositories werden kurz an UML-Klassendiagrammen erläutert. Dieser Artikel ist ein Teil der Artikelserie zu einem [Adressbuch-SpringBoot-Projekt](https://oer-informatik.de/springboot-projekt). Weiter geht es dann mit dem Test des Modells über das Repository: [Testen des Repositories](https://oer-informatik.de/sbb04_repository-test).

### Konfiguration und Abhängigkeiten

Der Ablauf beim Einfügen einer neuen Komponente ist bei Spring(Boot) weitgehend identisch:

- Abhängigkeiten in der `pom.xml` (Maven) bzw. `build.gradle` (Gradle) eintragen

- neue Klasse erzeugen, die mit einer `@Component`-Annotation annotiert wird

- Konfigurationen anpassen - z.B. über `application.properties` im Ordner `src/main/ressources`

Um ein Model mit zugehörigem Datenspeicher (Persistenzschicht) zu erstellen müssen zwei Abhängigkeiten ergänzt werden:

- Spring Data JPA

- Eine Persistence Unit - hier der Einfachheit halber zunächst die in-memory-Datenbank H2

Folgende Abschnitte müssen  in der `pom.xml` vorhanden sein:

```xml
<dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
<dependency>
      <groupId>com.h2database</groupId>
      <artifactId>h2</artifactId>
      <scope>runtime</scope>
</dependency>
```

(NB: händisch reinkopieren, IJ: kopieren oder über Code/Generate => Dependency nach "spring-boot-starter-data-jpa" und "com.h2database" suchen)

### Gliederung des Projekts in Packages

Es bestehen für ein Projekt zwei grundsätzliche Ansätze, wie die einzelnen Klassen in Packages strukturiert werden:

- Gliederung nach Begriffen der Problem- oder Fachdomäne: alle Klassen, die zu einem bestimmten Teil der Fachdomäne gehören werden in einem Package zusammengefügt. Also etwa alle Dateien, die eine Adresse verarbeiten.

- Gliederung nach Komponenteneingenschaften: Klassen, die einen gemeinsamen _Concern_ behandeln werden in Packages zusammengefasst: also etwa alle Controller, alle Models und alle Views.

Für beide Varianten gibt es Vor- und Nachteile. Wir erstellen hier eine Struktur basierend auf der Fachdomäne: alle Adress-Klassen werden in einem Package zusammengefasst.

Hierzu muss zunächst ein neues Package erstellt werden, also zunächst ein neuer Ordner unterhalb von `addressbook` (Rechtsklick) mit Namen `address`:

![Neuen Ordner (address) unterhalb von addressbook anlegen](images/Model/01_newfolder.png)

![Neue Javadatei(Address.java) in diesen Ordner speichern](images/Model/02_newfile.png)


### Die Modell-Klasse

Die neu angelegte Klasse hat bereits einen Rumpf:

```java
package de.csbme.ifaxx.addressbook.address;

public class Address {

}
```

Dieses Modell wird als _POJO_ umgesetzt, das zusätzlich mit Annotationen versehen wird. Als _POJO_ (_Plain Old Java Object_) im engeren Sinne werden einfache Java-Klassen verstanden, die nur von `Object` erben, keine Interfaces implementieren und nicht annotiert sind. Häufig verfügen _POJOs_ nur über Attribute, Getter und Setter.

Ein Model wird mit der Annotation `@Entity` versehen - und damit hat JPA im Prinzip schon alle Information, die es benötigt.

```java
@Entity
public class  Address { /* */ }
```

Damit die Annotation `@Entity` bekannt ist muss nur noch die Persistenzumgebung importiert werden. Schließlich liegt die Annotation im Paket javax.persistence:


```java
import javax.persistence.Entity;
```


Trägt die Tabelle in der Datenbank einen anderen Namen als die Klasse (z.B. das deutsche Adressen statt englisch _address_), so kann dieser mit der Annotation `@Table(name = "Adressen")` festgelegt werden.

Es folgt die Deklaration der Attribute:

```java
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
private Long id;

private String firstName;

private String lastName;

private String street;

private String streetAdditional;

private String postalCode;

private String city;

private String country;
```

Auch die Felder können annotiert werden, i.d.R. ist dies aber nur beim Primärschlüssel (`id`) erforderlich.
Die Annotation `@Column` bietet einige Parameter, die interessant sein könnten:

- `String name`: Der Name des Attributs in der Tabelle (Spaltenname) weicht vom Klassenattributnamen ab

- `boolean unique`: bei `unique=true` muss der Wert einzigartig sein

- `boolean nullable`: bei `nullable=false` darf der Attributwert nicht `NULL` sein

- `boolean updatable / insertable`: Attribut wird in UPDATE / INSERT-Befehlen ignoriert, wenn diese _flag_ expizit auf `false` gestellt wird (`updatable=false`)

- `String table`: Wenn dieses Attribut in einer anderen Tabelle als die Primäre Tabelle gespeichert wird, kann der Name hier angegeben werden.

- `int length`: Wenn die Zeichenlänge eines Strings von max. 255 abweicht, dann dies hiermit angegeben werden.

- `int precision / scale`: Gibt die Anzahl der Nachkommastellen an, falls der Datentyp `Decimal` genutzt wird (muss analog zur Angabe per DDL gesetzt werden)

Eine sinnvolle Annotation eines Attributs könnte also sein:

```java
    @Column(nullable = false, unique = false, name="Nachname")
    private String lastName;
```

Darüber hinaus müssen Konstruktoren eingefügt werden: ein Default-Konstruktor (weil die JPA es verlangt, dieser wird jedoch nicht genutzt) und ein Konstruktor, der alle Attribute setzt, die nicht mit `@GeneratedValue` annotiert sind:

VSCode bietet uns im Kontextmenü (rechte Maustaste im Editorfenster beim Klick auf einen Attributnamen) unten den Punkt "Quellaktion" an, der einiges vereinfachen wird:

![Kontextmenü Quellaktion](images/Model/03_quellaktion.png)

Damit lassen sich zunächst alle Getter und Setter, später auch der Konstruktor setzen.

![Getter und Setter automatisch setzen](images/Model/04_gettersetter.png)

Im Fall des Konstruktors sollten alle Attribute bis auf id ausgewählt werden.

Den Default-Konstruktor ohne Parameter müssen wir manuell eingeben. Die beiden Konstruktoren wären also etwa:

```java
public Address(){
}

public Address(String firstName, String lastName, String street, String streetAdditional,
        String postalCode, String city, String country) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.street = street;
    this.streetAdditional = streetAdditional;
    this.postalCode = postalCode;
    this.city = city;
    this.country = country;
}
```

Wir ergänzen noch einen abgekürzten Konstruktor, damit wir auch Objekte nur mit Vorname und Nachname bilden können:

```java
public Address(String firstName, String lastName){
    this(firstName, lastName, "Musterstraße", "123",
    "10435", "Berlin", "Deutschland");
}
```

Es gehört zum guten Stil, dass man die `toString()`-Methode ebenso überschreibt und alle relevanten Attribute ausgibt (Auch hier hilft die Codegenerierung von oben):

```java
@Override
public String toString() {
    return "Address [city=" + city + ", country=" + country + ", firstName=" + firstName + ", id=" + id
            + ", lastName=" + lastName + ", postalCode=" + postalCode + ", street=" + street + ", streetAdditional="
            + streetAdditional + "]";
}
```

### Erstellen eines neuen Repositories

Eigentlich weiß Springboot jetzt alles, was es wissen muss, um Instanzen von `Address` zu erzeugen und deren Attribute in einer Datenbank zu speichern. Es fehlen nur noch Methoden, die die **C**reate, **R**ead, **U**pdate und **D**elete- Aktionen (CRUD) für die Datenbank implementieren. Eine Klasse, die genau das tut, nennt sich ein _Repository_.

Repositories sind Komponenten, die bestimmte fachliche Klassen verwalten. Um die Implementierung müssen wir uns aber nicht mehr kümmern, das übernimmt das Framework. Wir legen lediglich ein Interface an, SpringBoot implementiert die fehlenden Methoden an Hand des übergebenen Models zur Laufzeit automatisch.

Im `address`-Ordner legen wir eine neue Datei `AddressRepository.java` an. In der Datei definieren wir ein Interface, legen fest, dass es vom `CrudRepoistory` erbt (eine Vorlage von Spring), sowie, dass die generische Typparameter das `Address` und der id-Typen `Long` sind:

```java
package de.csbme.ifaxx.addressbook.address;
import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Long> {}
```  

Der Rest passiert per Zauberhand: Das Spring-Framework implementiert (vereinfacht gesagt) zur Laufzeit eine konkrete Klasse, die unsere Address-Klasse nutzt:

![](plantuml/AddressRepository.png)


Es mag erstaunen, dass diese Repository-Interfaces nie direkt implementiert werden müssen: Spring sucht zunächst Implementierungen, versucht dann über _Spring Data_ Abfragen automatisch zu generieren oder zuletzt die Methoden der Referenzimplementierung `SimpleJpaRepository` zu nutzen.

![](plantuml/RepositoryInterface.png)

### Vertiefung der Annotationen für das Modell

#### Erweiterung der Annotationen für Attribute

Es gibt eine ganze Reihe weiterer Annotationen, die verwendet werden können. Zum Teil sind diese an eine spezielle Implementierung der JPA (i.d.R. Hibernate) gebunden und nicht generell gültig.

- `@GeneratedValue(strategy = GenerationType.AUTO)`: hier können auch noch die Strategien `IDENTITY`, `SEQUENCE`, und `TABLE` ausgewählt werden.

- `@NotNull`: kann auf Methoden, Attribute oder Parameter angewendet werden (bei Attributen kann auch `@Column(nullable=false)` gesetzt werden)

- `@NaturalId`: Sofern das Domänenmodell über einen natürlichen Schlüssel verfügt kann dieser hiermit annotiert werden.

- `@NotEmpty`: für Zeichenketten - verhindert Leerstrings.

- `@Size(min = 10, max = 32)`: gibt an, wie lange eine Zeichenkette minimal / maximal sein darf

### Nächste Schritte

Dieser Artikel ist ein Teil der Artikelserie zu einem [Adressbuch-SpringBoot-Projekt](https://oer-informatik.de/springboot-projekt).

Als nächstes sollte das Modell und das Repository einmal benutzt werden, um die Funktionalität zu verstehen und zu prüfen, ob alles so klappt, wie es soll. Dazu muss

- Eine SpringBoot-Testumgebung geschaffen

- für die wesentlichen Methoden (CRUD) jeweils ein Test geschrieben werden

Weiter geht es also mit dem Test des Modells über das Repository: [Testen des Repositories](https://oer-informatik.de/sbb04_repository-test).
