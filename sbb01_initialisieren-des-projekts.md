## Ein erstes SpringBoot Projekt mit VSCode erzeugen


<script type="text/javascript" src="https://oer-informatik.gitlab.io/service/ci-pipeline/src/oer-scripts.js" id="oer-script-js"></script>

<span class="hidden-text" title="mastodonurl">https://bildung.social/@oerinformatik/</span>

<span class="hidden-text" title="arcticleurl">https://oer-informatik.de/sbb01_initialisieren-des-projekt</span>


> **tl/dr;** _(ca. 6 min Lesezeit): Mit dem SpringInitializr eine neues Projekt konfigurieren und anlegen, in der Projektstruktur zurechtfinden, einen "Hello World"-Webservice implementieren und starten. Dieser Artikel ist ein Teil der Artikelserie zu einem [Adressbuch-SpringBoot-Projekt](https://oer-informatik.de/springboot-projekt). Weiter geht es dann mit einer API, die ständig "selber..." sagt [Kleiner Bruder API](https://oer-informatik.de/sbb02_kleiner-bruder-api)._

Das Spring-Framework ist sehr flexibel und kann bis ins kleinste Detail konfiguriert werden. Daher muss für ein neues Spring-Projekt zunächst in einer Start-Konfiguration vorgegeben werden, welche Programmiersprache genutzt werden soll, wie das Projekt heißen soll usw. Hierzu gibt es ein Tool - den [Spring Initializr](https://start.spring.io/#!type=gradle-project&language=java&platformVersion=3.3.3&packaging=jar&jvmVersion=21&groupId=de.csbme.ifaxx&artifactId=addressbook&name=addressbook&description=Addressbook%20Backend&packageName=de.csbme.ifaxx.addressbook&dependencies=web). Wir erzeugen das Projekt später direkt in der IDE, die Website des Spring Initializrs hilft uns aber, eine Übersicht zu erhalten zu allen Eingaben, die wir Schritt für Schritt toolgestützt vornehmen werden:

![Der Spring Initializr als Webservice. Wenn keine Spring-IDE genutzt wird kann diese Konfiguration auch über [diesen link](https://start.spring.io/#!type=gradle-project&language=java&platformVersion=3.3.3&packaging=jar&jvmVersion=21&groupId=de.csbme.ifaxx&artifactId=addressbook&name=addressbook&description=Addressbook%20Backend&packageName=de.csbme.ifaxx.addressbook&dependencies=web) geladen werden](images/SpringInitializr/01_SpringInitializr.png)


Die Spring Extensions von VS Codium helfen uns dabei, die Konfiguration zu erstellen. Falls einzelne Schritte nicht klar sind, hilft vielleicht
diese ausführlichere Anleitung^[[Spring Boot Extension und Nutzung des Spring Initializrs mit VS Code](https://code.visualstudio.com/docs/java/java-spring-boot)] dabei, ein erstes Projekt zu erstellen.

Wenn keine Spring-IDE genutzt wird, kann diese Konfiguration auch über [diesen link des SpringInitializrs](https://start.spring.io/#!type=gradle-project&language=java&platformVersion=3.3.3&packaging=jar&jvmVersion=21&groupId=de.csbme.ifaxx&artifactId=addressbook&name=addressbook&description=Addressbook%20Backend&packageName=de.csbme.ifaxx.addressbook&dependencies=web) geladen werden.

Um den Spring Initializr in VSCode zu nutzen geht man wie folgt vor:

* Commands per Tastenkombination `Str-Shift-P` öffnen, dort wahlweise

 -  "**Spring Initializr: Create a Maven Project**" (unten) eingeben (empfiehlt sich für Einsteiger)

 -  "**Spring Initializr: Create a Gradle Project**" (oben) eingeben (empfiehlt sich für erfahrene Gradle-Nutzer)

![Spring Initializr starten](images/SpringInitializr/02_CreateSpringboot_Maven.png)


* die letzte stabile Version wählen (hier: 3.3.3)

![Spring Boot Version wählen](images/SpringInitializr/03_Version.png)

* die genutzte Programmiersprache wählen (Java)

![Programmiersprache Java wählen](images/SpringInitializr/04_Programmiersprache.png)

* eine Group-ID wählen (Paketbezeichnung in Java, Aufbau wie ein umgekehrter FullQualifiedDomainName FQDN). Ich wähle `de.csbme.ifaxx`

![GroupID eingeben](images/SpringInitializr/05_GroupID.png)

* eine Bezeichnung für das Produkt (Artefakt) wird noch verlangt. Ich wähle `addressbook`

![ArtifactID eingeben](images/SpringInitializr/06_ArtifactID.png)

* es muss noch ausgewählt werden, ob ein JAR (Archiv der Java-Klassendateien) oder eine WAR-Datei (Spezielles JAR, das für v.a. für Servlets genutzt wird) benutzt werden soll. Das Projekt wird als JAR erstellt.

![JAR- oder WAR-Datei?](images/SpringInitializr/07_JarWar.png)


* welche Java-Version soll genutzt werden? Empfehlenswert ist jeweils die letzte LTS-Datei (wie oben installiert), momentan ist das noch Java 21. Bei der Gelegenheit kann nochmal überprüft werden, ob `java --version`, `mvn -v` / `gradle -v` und `JAVA_HOME` jeweils auf diese Version zeigen.

![Welche JavaVersion?](images/SpringInitializr/08_JavaVersion.png)

* abschließend werden noch die wesentlichen Module angegeben, die das Projekt nutzen soll. Wir benötigen zunächst nur "Spring Web" (scrollen! es steht weiter unten).

![Dependencies : Spring Web auswählen](images/SpringInitializr/09_SpringWeb.png)

Es folgt die Abfrage, wo der Projektordner angelegt werden soll. Wichtig: VSCode ist manchmal etwas uneinsichtig, wenn im Pfad Umlaute vorkommen.

Das Projekt kann direkt über ein kleines Pop-up in der unteren rechten Ecke geöffnet werden:

![Öffnen des Projekts](images/SpringInitializr/11_open.png)

Doch davor muss man dem Autor noch das Vertrauen schenken:

![Nachfrage der IDE, ob man dem Autor vertraut](images/SpringInitializr/12_autorvertrauen.png)

### Im Springboot-Projekt zurechtfinden

Für den Anfang sind drei Dateien in dem neu erstellten Projekt wichtig: die Config-Datei des Build-Tools (Maven: `pom.xml`, Gradle: `build.gradle`), die `AddressbookApplication.java` (das eigentliche Programm) und die `application.properties` (Konfiguration von Spring)

<span class="tabrow" >
   <button class="tablink" data-tabgroup="buildTool" data-tabid="maven" onclick="openTabsByDataAttr('maven', 'buildTool')">Maven</button>
   <button class="tablink tabselected" data-tabgroup="buildTool" data-tabid="gradle" onclick="openTabsByDataAttr('gradle', 'buildTool')">Gradle</button>
   <button class="tablink" data-tabgroup="buildTool" data-tabid="all" onclick="openTabsByDataAttr('all', 'buildTool')">_all_</button>
    <button class="tablink" data-tabgroup="buildTool" data-tabid="none" onclick="openTabsByDataAttr('none', 'buildTool')">_none_</button>
</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="maven" style="display:none">

* in der `pom.xml`-Datei finden sich alle Einstellungen, die wir gerade mithilfe von Spring Initilizr gemacht haben. Hier müssen wir zunächst nichts anpassen:

![Die pom.xml-Datei](images/SpringInitializr/13_projectexplorer.png)

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="gradle" >

* in der `build-gradle`-Datei finden sich alle Einstellungen, die wir gerade mithilfe von Spring Initilizr gemacht haben. Hier müssen wir zunächst nichts anpassen:

![Die build.gradle-Datei](images/SpringInitializr/13_projectexplorer_gradle.png)

</span>

* die  `application.properties` findet sich im Verzeichnis `src/main/ressources`. Diese Datei ist wichtig, wenn der Port geändert werden muss, über den die Springboot-App erreichbar ist (also z.B. `http://localhost:8080`).

Ob ein Port bereits belegt ist, findet man z.B. über die PowerShell mit dem Einzeiler: `Get-Process -Id (Get-NetTCPConnection -LocalPort 8080).OwningProcess` heraus.
In die bis dahin leere Datei muss ein Einzeiler mit dem neuen Port eingefügt werden. Ich nutze hier immer:

```ini
server.port=8085
````

 * Jetzt muss nur noch das Programm `AddressbookApplication.java`so angepasst werden, dass es eine Ausgabe erzeugt.

![Anpassungen in der Application](images/SpringInitializr/14_Application.png)

 Hierzu muss (von unten nach oben):

 * Eine Methode `helloWorld()` angefügt werden, die die Ausgabe vornimmt,

 ```java
 @RequestMapping("/hello")
 String helloWorld() {
   return "Hello World! \n";
 }
 ```

 * die Klasse `AddressbookApplication` mit der Annotation `@RestController` versehen werden, damit Spring weiß, dass diese Klasse für Anfragen (Requests) zuständig ist,

 * die entsprechenden Imports angefügt werden - z.B. in dem über die ToolTips die entsprechenden Imports ergänzt werden.

![Annotations über ToolTips](images/SpringInitializr/15_tooltip.png)

### Der spannende Augenblick

<span class="tabrow" >
   <button class="tablink" data-tabgroup="buildTool" data-tabid="maven" onclick="openTabsByDataAttr('maven', 'buildTool')">Maven</button>
   <button class="tablink tabselected" data-tabgroup="buildTool" data-tabid="gradle" onclick="openTabsByDataAttr('gradle', 'buildTool')">Gradle</button>
   <button class="tablink" data-tabgroup="buildTool" data-tabid="all" onclick="openTabsByDataAttr('all', 'buildTool')">_all_</button>
    <button class="tablink" data-tabgroup="buildTool" data-tabid="none" onclick="openTabsByDataAttr('none', 'buildTool')">_none_</button>
</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="all" style="display:none">

_Hinweis: Tooltips (mouseover) geben Auskunft, zu welchem Tab (oben) der jeweilige Bereich gehört._

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="none"  style="display:none">

_Hinweis: Kein Tab ausgewählt, Einträge auf der Tableiste auswählen._

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="maven" style="display:none">

Ausgeführt wird die Datei z.B. über das Menü (Ausführen - Debugging starten) oder über den "Play"-Button oben rechts:

![Das Programm starten](images/SpringInitializr/16_play.png)

</span>

<span class="tabs" data-tabgroup="buildTool" data-tabid="gradle" >

Wenn die Extension "Gradle for Java" installiert ist, lässt sich das Projekt komfortable über das Menü "Tasks / application / bootRun" ausführen. (Sollte das nicht erscheinen, wohl aber "init", muss möglicherweise vorher ein "init"-Durchlauf erfolgen)

![Den gradle-build starten](images/SpringInitializr/16_play_gradle.png)

</span>

Es folgt eine Weile Download-Meldungen, und ein bisschen ASCII-Art:

```
 *  Executing task: gradle: bootRun 


> Task :bootRun

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/

 :: Spring Boot ::                (v3.3.3)

2024-08-29T20:45:50.556+02:00  INFO 8784 --- [addressbook] [           main] d.c.i.a.AddressbookApplication           : Starting AddressbookApplication using Java 21.0.1 with PID 8784 (C:\Users\hanne\Projekte\addressbook\build\classes\java\main started by hanne in C:\Users\hanne\Projekte\addressbook)
2024-08-29T20:45:50.560+02:00  INFO 8784 --- [addressbook] [           main] d.c.i.a.AddressbookApplication           : No active profile set, falling back to 1 default profile: "default"
2024-08-29T20:45:52.217+02:00  INFO 8784 --- [addressbook] [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port 8085 (http)
2024-08-29T20:45:52.267+02:00  INFO 8784 --- [addressbook] [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2024-08-29T20:45:52.267+02:00  INFO 8784 --- [addressbook] [           main] o.apache.catalina.core.StandardEngine    : Starting Servlet engine: [Apache Tomcat/10.1.28]
2024-08-29T20:45:52.365+02:00  INFO 8784 --- [addressbook] [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2024-08-29T20:45:52.366+02:00  INFO 8784 --- [addressbook] [           main] w.s.c.ServletWebServerApplicationContext : Root WebApplicationContext: initialization completed in 1690 ms
2024-08-29T20:45:52.892+02:00  INFO 8784 --- [addressbook] [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port 8085 (http) with context path '/'
2024-08-29T20:45:52.899+02:00  INFO 8784 --- [addressbook] [           main] d.c.i.a.AddressbookApplication           : Started AddressbookApplication in 3.247 seconds (process running for 3.976)
2024-08-29T20:46:10.068+02:00  INFO 8784 --- [addressbook] [nio-8085-exec-1] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring DispatcherServlet 'dispatcherServlet'
2024-08-29T20:46:10.068+02:00  INFO 8784 --- [addressbook] [nio-8085-exec-1] o.s.web.servlet.DispatcherServlet        : Initializing Servlet 'dispatcherServlet'
2024-08-29T20:46:10.069+02:00  INFO 8784 --- [addressbook] [nio-8085-exec-1] o.s.web.servlet.DispatcherServlet        : Completed initialization in 1 ms
<==========---> 80% EXECUTING [59s]
> :bootRun
```

Wenn das Programm bereits einmal läuft (und somit den Port belegt) oder ein anderer Dienst auf dem Port läuft muss gegebenenfalls der Port getauscht (siehe oben) oder der jeweilige Service beendet werden. Man erkennt diese Situation an dieser Meldung:

```
Error starting ApplicationContext. To display the conditions report re-run your application with 'debug' enabled.
2023-09-04T22:22:00.153+02:00 ERROR 13564 --- [           main] o.s.b.d.LoggingFailureAnalysisReporter   :

***************************
APPLICATION FAILED TO START
***************************

Description:

Web server failed to start. Port 8085 was already in use.

Action:

Identify and stop the process that's listening on port 8085 or configure this application to listen on another port.
```

Mehrere eigene Instanzen lassen sich z.B. rechts am Terminal-Fenster erkennen und löschen (Mülleimer).

![Mehrere Instanzen erkennen und löschen im Terminal-Fenster](images/SpringInitializr/17_stopInstance.png)

### Lorbeeren ernten: die App im Browser

Wenn das alles geklappt hat ist es an der Zeit, die Lorbeeren zu ernten und im Browser die API zu testen: ein Aufruf von `http://localhost:8085/hello` sollte das erwünschte "Hello World!" zutage fördern:

![Das Ergebnis: Hello World im Browser](images/SpringInitializr/18_browser.png)

Wunderbar. Noch nicht viel. Aber es klappt. Falls es nicht klappt ist vielleicht noch der Port 8080 eingestellt (s.o.)? Was passiert bei `http://localhost:8080/hello`?

### Nächster Schritt

Dieser Artikel ist ein Teil der Artikelserie zu einem [Adressbuch-SpringBoot-Projekt](https://oer-informatik.de/springboot-projekt).

Weiter geht es dann mit einer API, die ständig "selber..." sagt: der  [Kleiner Bruder API](https://oer-informatik.de/sbb02_kleiner-bruder-api).

### Links und weitere Informationen

- [Spring Initializr Website](https://start.spring.io/)
